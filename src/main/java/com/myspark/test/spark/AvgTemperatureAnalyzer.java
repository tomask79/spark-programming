package com.myspark.test.spark;

import scala.Tuple2;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public final class AvgTemperatureAnalyzer {
  private static final Pattern SPACE = Pattern.compile(" ");

  public static void main(String[] args) throws Exception {
    SparkConf sparkConf = new SparkConf();
    sparkConf.set("spark.cores.max", "1");
    sparkConf.set("spark.executor.memory", "2048M");
    sparkConf.set("spark.driver.memory", "1024M");
    sparkConf.setAppName("JavaWordCount");
    sparkConf.setMaster("spark://tomask79.local:7077");
    JavaSparkContext ctx = new JavaSparkContext(sparkConf);
    ctx.addJar("/Users/tomask79/Documents/workspace-sts/apache-spark/spark/target/spark-0.0.1-SNAPSHOT.jar");
    JavaRDD<String> lines = ctx.textFile("hdfs://localhost:9000/user/weather.txt", 1);
       
    JavaRDD<String> parsedTemperatures = lines.map(new Function<String, String>() {
		private static final long serialVersionUID = 1L;

		public String call(String v1) throws Exception {
			final String arr[] = SPACE.split(v1);
			System.out.println("Reading temperature ["+arr[1]+"] from "+v1);
			return arr[1];
		}
	});
    

    JavaPairRDD<String, Integer> forGroup = parsedTemperatures.mapToPair(
    		new PairFunction<String, 
    		String, Integer>() {
  		private static final long serialVersionUID = 1L;

  		public Tuple2<String, Integer> call(String t) throws Exception {
  			return new Tuple2<String, Integer>("reducer", Integer.parseInt(t));
  		}
    });

      
    JavaPairRDD<String, Integer> counts = forGroup.reduceByKey(
    		new Function2<Integer, Integer, Integer>() {
		private static final long serialVersionUID = 1L;

		public Integer call(Integer v1, Integer v2) throws Exception {
			  System.out.println("Agregatting "+v1+" plus "+v2);
	          return v1 + v2;
		}
    });


    Tuple2<String, Integer> sumTemperatures = counts.first();    
    final Integer sum = sumTemperatures._2;
    final long count = parsedTemperatures.count();
    final double avg = (double) sum / count;
    System.out.println("Average temperature "+avg);
      
    JavaRDD<String> result = lines.filter(new Function<String, Boolean>() {
		private static final long serialVersionUID = 1L;

		public Boolean call(String v1) throws Exception {
			final String arr[] = SPACE.split(v1);
			long temperature = Long.parseLong(arr[1]);
			return temperature <= avg;
		}
	});
    
    List<String> resultList = result.collect();
    for (String item: resultList) {
    	System.out.println("Result item: "+item);
    }
      
    ctx.stop();
  }
}