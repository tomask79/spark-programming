# Programming with Apache Spark #

I finally found a time to try another dive into the programming with Apache Spark. We will try to solve simple task with JavaRDD API. First of all, if you are not familiar with how to start Apache Spark cluster and how to submit a task to it then I highly recommend to read my previous tutorial about Apache Spark:

```
https://bitbucket.org/tomask79/apache-spark-maven/overview
```

## Apache Spark JavaRDD and task to solve ##

Well, I'm saying to everyone around me that if you don't know map-reduce concept then you can't understand Apache Spark. To prove it, let's solve simple task. Assume that we're having following text file with temperatures in towns:


```
Prague 35
Madrid 40
Berlin 20
Paris 15
Rome 25
```

located at Apache Hadoop HDFS file system and we need to write simple JavaRDD Apache Spark program **to print rows with towns having temperature lower then average temperature from the whole set**. 

## JavaRDD API and MapReduce, is there a difference? ##

Well actually **it's not!** To solve previously mentioned task, we need to split our problem into following parts:


* First, we need to write JavaRDD program to count the whole sum of temperatures and average value.
* Then we're going to print rows with temperatures lower then computed average.


### MapReduce solution concept ###

If we'd use Spring Data for Hadoop or simple Apache Hadoop API designated for map-reduce programs then our solution would be:

* Map function would be creating keys [K,V] = 'reducer', town.temperature
* Reduce function would receive previous keys and sum the temperatures from the whole set and compute the average temperature.
* Chained map-reduce task would print the result of towns having temperature lower then average.

### Apache Spark JavaRDD solution ###

* First we need to get JavaRDD set of temperatures from all rows by applying map function to the input RDD set:


```
    JavaRDD<String> parsedTemperatures = lines.map(new Function<String, String>() {
		private static final long serialVersionUID = 1L;

		public String call(String v1) throws Exception {
			final String arr[] = SPACE.split(v1);
			System.out.println("Reading temperature ["+arr[1]+"] from "+v1);
			return arr[1];
		}
	});
```
(similary to mapreduce)

* Then we need to transform this RDD set into the CONSTANT.row.temperature form to prepare the data into reducer:


```
JavaPairRDD<String, Integer> forGroup = parsedTemperatures.mapToPair(
    		new PairFunction<String, 
    		String, Integer>() {
  		private static final long serialVersionUID = 1L;

  		public Tuple2<String, Integer> call(String t) throws Exception {
  			return new Tuple2<String, Integer>("reducer", Integer.parseInt(t));
  		}
    });
```

* With this dataset, we're having data prepared for reducer, which will aggregate all the temperatures


```
JavaPairRDD<String, Integer> counts = forGroup.reduceByKey(
    		new Function2<Integer, Integer, Integer>() {
		private static final long serialVersionUID = 1L;

		public Integer call(Integer v1, Integer v2) throws Exception {
			  System.out.println("Agregatting "+v1+" plus "+v2);
	          return v1 + v2;
		}
    });
```
(again, like in map-reduce concept)

To see how Spark reducer for works look at log:

```
Reading temperature [35] from Prague 35
Reading temperature [40] from Madrid 40
Agregatting 35 plus 40
Reading temperature [20] from Berlin 20
Agregatting 75 plus 20
Reading temperature [15] from Paris 15
Agregatting 95 plus 15
Reading temperature [25] from Rome 25
Agregatting 110 plus 25
```
**Spark actually runs first three functions map, mapToPair and reduceByKey in parallel!** One of the benefits of DAG graph analyzer of composing Spark tasks together!

Second part of solution is printing all towns having temperature lower then average:


```
    Tuple2<String, Integer> sumTemperatures = counts.first();    
    final Integer sum = sumTemperatures._2;
    final long count = parsedTemperatures.count();
    final double avg = (double) sum / count;
    System.out.println("Average temperature "+avg);
      
    JavaRDD<String> result = lines.filter(new Function<String, Boolean>() {
		private static final long serialVersionUID = 1L;

		public Boolean call(String v1) throws Exception {
			final String arr[] = SPACE.split(v1);
			long temperature = Long.parseLong(arr[1]);
			return temperature <= avg;
		}
	});
    
    List<String> resultList = result.collect();
    for (String item: resultList) {
    	System.out.println("Result item: "+item);
    }
```

Let's explain this code a little bit:

* By counts.first() we read the sum of all temperatures from reducer
* We used count function to get the count of all rows in JavaRDD input set.
* We used JavaRDD filter function to filter out the towns with higher temperature then average.
* We used JavaRDD collect function to print the result.


If you run this program, you should get result like:

```
16/03/03 21:02:26 INFO DAGScheduler: Job 1 finished: count at AvgTemperatureAnalyzer.java:85, took 0,094561 s

Average temperature 27.0
.
.
Result item: Berlin 20
Result item: Paris 15
Result item: Rome 25

16/03/03 21:02:26 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/metrics/json,null}
```


# Conclusion #

From my point of view is Apache Spark much more friendly looking map-reduce programming, even if the concepts are the same. I bet you understood that we needed more then one iteration through the JavaRDD input, but with map-reduce you need to figure out for example how to pass result from previous map reduce task to the next one, with Apache Spark one input iteration ends with new RDD set where you can just apply another functions, everything driven from master node...Isn't this cool?